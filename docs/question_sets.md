# Creating a Question Set

The question set library was created because we saw a need for data comparability between data sets and between surveys. If you have expertise in an area that may be useful to others and want to contribute to the question set library the first step is to create a survey that only contains the questions you want in the set. 

When you've finished creating and thoroughly testing your question set survey, select the three dots next to the survey name and click `Add to library`. The survey will then prompt you to add information (description, maintainers, applications) about the question set before adding it to the library. Again, please make sure you have thoroughly tested the survey logic before adding it to the library. It is much easier to make changes before adding it to the library. 

![add to library](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/add_to_library.JPG)

After adding the survey to the library you can go through and decide if you want to allow question set users to hide or modify each question. Think carefully about which questions are modifiable as this will change the data sets that you recieve. 

![allow modify](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/images/allow%20modify.png)