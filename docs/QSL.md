# Pull in an existing question set
You can use question sets that have already been created by other groups by selecting `Search question library` in the list of question types. This allows for better data comparability and saves time in the survey creation process. If you know of another group collecting similar data make sure to check here before you start creating a survey from scratch!

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/add_question.png)  

After selecting `Search question library` some of the question sets will be listed, or you can enter key terms in the search bar.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/search_library.JPG)

Selecting a set will open a description and preview along with the maintainers. If you have questions about the set you should contact the maintainers. 

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/library_example.JPG)

Select `Add to survey` if you want to use the question set. It will appear as a set of green highlighted questions in your survey. The survey creator has the ability to change some things in the question set such as the labels, but data names will stay the same as in the original question set. Some questions within the set will have a checkbox option to hide the question which makes it invisible to the submitter.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Question%20types/add_set.JPG)

 