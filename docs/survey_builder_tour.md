# Tour of Survey Builder

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

### Survey Outline
1. **Name** the survey
2. **Assign survey to group** - survey submissions will automatically be assigned to this group if the user is not signed in or has not chosen an active group. Otherwise, the submission will be assigned to the `active group` chosen by the user. 
3. **Import/Export** Surveys has a json.
4. **Delete** draft surveys
5. **Save** - saves the survey as a draft, the changes will not be visible to users filling out surveys.
6. **Publish** - publishes the survey changes, changes will be visible to users filling out surveys.
7. **Update** - updates the survey, changes will be visible to users filling out surveys, but the change does not create a new version of the survey.
8. Use the `plus` icon to **Add a new question.**
![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Tour%20of%20Survey%20Builder/survey_outline.png)

### Question Properties  
1. Enter **data name** -  this will be passed on in the json object and will be the column header in the results table.
2. **Label** - This text is above the question boxes.
3. **Hint** - This text will be directly above multiple choice answer options or will be in the answer box for text, number and dropdown questions. 
4. **More Info** - This text is below the answer box or the multiple choice options list and should only be used to provide additional directions when the Label and Hint are not sufficient.  
5. **Default Value** - Some question types allow for a default value. This value will show up when the user is filling out the survey, but they will be able to change it if needed.
6. Make a question **Required** - By checking the required box, a user cannot advance in the survey until they have answered the question
7. Keep data **Private** - If this box is checked, answers to these questions will not be visible to the public. Only the person who submitted the survey or an administrator for the group can see private data.
8. Choose **Advanced** setting like relevance or API compose to add survey logic or make API calls to other services.
9. Select options and settings for specific question types (see next section).  

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Tour%20of%20Survey%20Builder/question_properties.png)

### Survey Preview
Preview how the survey will look on computer or mobile devices and test relevance logic.
