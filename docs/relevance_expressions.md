# Relevance Expressions

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

Relevance expressions are the most common advanced feature used in SurveyStack surveys. This feature allows survey creators to deploy logic expressions to only show users the questions that are relevant to them.

Relevance expressions use javascript and are very flexible. Below are some examples of the most common use cases and associated relevance expressions.

We are going to use basic javascript if statements, as well as some special functions "checkIfAny" and "checkIfNone".  checkIfAny and checkIfNone are smarter because they not only check if an answer is there (or not), but they also confirm that the previous question was relevant.  This is important if you are making a dependency on a question which is either optional (and can be skipped) or itself has a dependency, and therefore may be unanswered or answered but irrelevant.  In addition, they check both single answer AND multiple answer question types.   Therefore in general, **it is safest to use checkIfAny and checkIfNone when possible**.

**Use Case 1: Show a question or group of questions based on the PRESENCE of a specific answer from multi-select questions OR single-answer question that is not required (so the answer may be empty or null).**

```
function relevance(submission,survey,parent) {
if (utils.checkIfAny(parent.activity, 'planted')) {
        return true
    }
return false
}
```

Where `activity` is the `data_name` of the question being referenced and "planted" is the `Value` of the answer. You can use parent as a substitute for the full data name as long as the question you are referencing is in the same group as the question you are creating the relevance expression for. Otherwise you will need to write out the full data name starting with submission.data.This question or group of questions will appear if any of the answers to the `activity` question is "planted."  Here we are use a special `utils` function which checks if any of the listed items are, in this case just 'planted', are present in the answer to the question 'activity'.

**Use Case 2: Show a question or group of questions on the ABSENCE of a specific answer from multi-select questions OR single-answer question that is not required (so the answer may be empty or null).**

```
function relevance(submission,survey,parent) {
if (utils.checkIfNone(parent.activity, 'planted')) {
        return true
    }
return false
}
```

Where `activity` is the `data_name` of the question being referenced and "planted" is the `Value` of the answer. This question or group of questions will appear BOTH if the 'activity' question is answered AND none of the answers provided are "planted."  Using this `utils` function, you could add additional options besides 'planted' and it would only return true if none of the listed options were included in the 'activity' answer. 

**Use Case 3: Show a question or group of questions if a previous question is itself relevant (even if it's unanswered).**

```
function relevance(submission,survey,parent) {
    if (utils.checkIfAny(parent.containers)) {
        return true
    }
return false
}
```

CheckIfAny, when provided no additional parameters, returns true if the question itself is relevant.

**Use Case 4: Show a question or group of questions if the number answer to a previous question is <, >, ===, or !== to a specific value.**

```
function relevance(submission,survey,parent) {
    if (parent.containers.value > 2) {
        return true
    }
return false
}
```

This question or group of questions will appear if the answer to the `containers` question is greater than 2.
  
#### Adding AND/OR statements to relevance expressions

**Using OR with `CheckIfAny` or `CheckIfNone` statements**

```
function relevance(submission,survey,parent) {
     if (utils.checkIfAny(parent.activity, 'planted', 'weeded')) {
        return true 
     }
return false
}
```

The question will be relevant if the user answers either "planted" **OR** "weeded"


**Using AND with to `CheckIfAny` or `CheckIfNone` statements**

```
function relevance(submission,survey,parent) { 
     if (utils.checkIfAny(parent.activity, 'weeded') && utils.checkIfAny(parent.activity, 'weeded')) {  
        return true 
     }
return false
}
```

The question will be relevant if the user answers "weeded" **AND** "watered"
