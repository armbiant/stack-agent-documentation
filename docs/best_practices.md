# Survey Best Practices

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

### Questions to think about before you start building your survey
**AUDIENCE**  

- **Who is filling it out?** Consider whether your audience has previous experience with this type of survey. If they have extensive knowledge then you don’t want to include lots of instructions that they have to click through. If most people will be filling this out for the first time you may want to include photos to help guide them through the process. Also consider what type of information about the user needs to be recorded. Do you need their contact information? Look through the options for who can fill out your survey. You may want to only allow logged in users or group members to fill out the survey. 

- **What’s their experience?** Are most people going to fill out the survey in field on a phone or sitting at their computer? If most people are going to be using their phone, use the mobile preview in the survey builder to get a better idea of what they will be seeing. Don’t use images that are hard to read on a small screen. Matrix questions can be difficult to view on mobile if the user needs to see each line of entries at once. If they are going to be sitting at their computer there is more opportunity for using instructional photos and guiding instructions, but you wouldn’t want to ask them to take a picture of their soil, for example.  

- **Frequency of use** Are they doing this 1 time or 100 times? If the user is going to be repeating the same process many times consider using survey logic to improve the user experience. For example if you were creating a survey for a soil lab test you could have a question at the beginning asking the user if they have been trained on the method before or if they have used the method many times. If they answer yes, you can use survey logic to skip over the very detailed instructions to reduce the number of clicks and the time it takes to complete the survey. You might also consider including more questions per page to limit the number of clicks required.  

- **Engagement** Is filling out this survey a requirement for the user? What is their incentive?  
<br>
These considerations can help you decide what question types are best suited to your needs. You want to keep the user engaged and consider when and why they might stop filling out the survey. Provide information on the purpose and what they will get out of the survey.  

**OUTCOME**  
**Theoretical level/qualitative**  
- What are you and your community trying to get out and expecting from the survey? What will the submissions inform?  
**Technical level**  
- How will this data be used? What format does it need to be in? Ex. We need a single calculated data field to get from survey to dashboard to user final downloaded csv.

### Creating the Survey

**General Design Rules**  
- Only show the user what they need to see  
- Respect the users time and intelligence  
- Don’t force users to do things you don’t need or want  
- Never enter data twice - ever!

**Naming conventions**  
- General Rule - Copy the structure of other similar surveys in your group, or even within other groups if it’s something that’s broadly applicable.

**Data Names**  
- The data name is what is getting saved in the json, this is what should stay consistent, the label is less important.  
- Note that the page data name is included in the full data name of a question.  
- Replicate data names in pages when applicable, ex. Instructions as a data name in multiple different pages

**Grouping Things Together**  
- The 'Group' question type buckets things together in builder. You can also apply logic to the whole group.  
- The 'Pages' question type looks and works the same as groups, but it also put things on the same page from the user perspective.

**How to decide what to put on the same page**  
- If the user is familiar with the work, then they likely want something efficient. ie. the directions available, but they don’t have to click through a bunch of pages especially if they’re repeating a process many times.

**Managing survey versions**  
- Each time you publish your survey a new version is created. Having too many versions can slow down the performance.  
- In order to optimize performance [delete old and unused versions of your survey](https://our-sci.gitlab.io/software/surveystack_tutorials/save_and_publish/)

**Other Tips**  
- If you know you need a script, create one in the scripts section of surveystack as a sort of filler before moving forward. Don’t know when you would need a script? See question types 
- Default status of any value in survey is null, it will stay this unless the user fills it out.  
- Ask yourself, at what point are people going to quit? What questions are going to cause this?

