# Clone or Delete Questions

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

Questions can be copied or deleted by selecting the `copy` or `delete` icons on a selected question. When a question is copied, `_copy` is added to the end of the `data_name`, and `Label` is renamed to `copy`. These can be edited to make the `Data name` unique and to update the label.

Groups can also be copied or deleted using the same process. When copying a group, **only the group `Data name` is changed, all questions in the group keep there name.** 

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Clone%20or%20Delete/duplicate.png "example of copying a question in the survey builder")