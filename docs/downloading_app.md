# Installing the App

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

The SurveyStack app is a Progressive Web App, which means you can choose to `install` it on your device similar to other apps, or you can choose to bookmark the page like any other website.

If you are using an android device, you can `Install` the app using **Chrome** by selecting the `INSTALL APP` button. 

***Note: Instructions for how to install the app on iPhone's or from Firefox are coming soon.***


