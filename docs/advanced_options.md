# Advanced Options

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

To access advanced options, select `ADVANCED` from the `Question Properties` Tab. Advanced question features include:

- **Relevance Expression**: These expressions are the most commonly used advanced feature. They allow the user to hide questions based on the responses to previous questions. Relevance expressions are simple javascript expressions and can be programmed to have flexible AND/OR logic. For more information about relevance, go to the relevance tutorial [here](https://our-sci.gitlab.io/software/surveystack_tutorials/relevance_expressions/).
- **Calculate and Constraint Expressions**: Coming soon
- **Api Compose Expression**: Uses javascript to allow you to send the survey, in it's entirety or in some modified form, to any publicly available API or to a non-public API which has been added to the SurveyStack Integrations.  In short - you can send your data where you want. For example, we use API compose expressions to push survey data into farmers FarmOS accounts in the Bionutrient Institute.

**IMPORTANT NOTE** When you code a relevance, calculate, constraint, or apiCompose script you do not have access to any javascript libraries!  You can only use the basic logic of javascript code plus a few special functions created to help you, which you can find [here](https://gitlab.com/our-sci/our-sci-pwa/-/blob/master/src/utils/sandboxUtils.js). These functions will be further described in the examples below.

### Finding Advanced Options

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Advanced%20Features/adding_relevance.png "screenshots of the survey builder showing where to find advanced options (click question, scroll down to the `ADVANCED` button and click.")

Once a user has selected advanced options, additional panes appear: one for coding expressions, a console log, and a `Shared Code` tab.  The `Shared Code` tab shows EXACTLY the format of the survey submission and it is updated in real time as you fill out a survey on the far right pane.  The survey submission is a JSON, and items in it are accessible in your code.  Each question and its outputs can be found in the `Shared Code` pane.  You will use this reference a lot when using Advanced options so get familiar with the submission structure!

A note on terminology - if you have not coded before you may not be familiar with the term JSON, and that’s ok! In this case a JSON is a data format used to store surveys and submissions. For the purposes of creating surveys with basic logic you will not need to be proficient in javascript, but if you want to learn more [W3 schools](https://www.w3schools.com/) and [Codeacademy](https://www.codecademy.com/) are great places to start!

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Advanced%20Features/shared_code.png "shared code pane showing the survey json object")
