## Edit a survey

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

To edit an existing survey:  
- Navigate to the survey you want to edit using the `Browse` tab in the side menu. It will default to showing surveys that are in your active group. You can either change your active group by selecting your profile in the top right corner, or you can go to `Browse all surveys` and search the survey by name.    
- At the survey landing page, select `EDIT`.  
- This will take you to the survey builder.

***Note: If the `EDIT` button is not visible, it means you do not have permission to edit this survey***

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Edit%20existing%20survey/edit_existing.png)


