# Edit a Submission

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

There are two ways to edit submissions:  
1. By editing the original submission on the same device from which it was submitted.  
2. By editing the submission directly from the database.

### Edit the original submission
1. Open up the [SurveyStack app](https://app.surveystack.io/)
2. Select `My Submissions` from the side menu.
3. Select the `SENT` tab.
4. Select the submission that you wish to edit.
5. Choose the reason for re-submiting the submission.
6. Select `EDIT ANYWAY`
7. This will open the *Survey outline*, navigate to the questions you need to edit, after completing your edits, `SUBMIT` the survey. 

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Edit%20a%20survey/confirm_edit.png)

### Edit from the Results tab
1. Select the survey for the submission you want to edit.
2. Select `RESULTS` from the upper right corner of the survey homescreen.
3. Search for the submission that you wish to edit. Find tips on how to navigate the Results page [here](view-results).
4. Select the submission.
5. Once a submission is checked, an `ACTIONS` bar will appear, select `RESUBMIT...`
6. Choose the reason for re-submiting the submission.
7. Select `EDIT ANYWAY`
8. This will open the *Survey outline*, navigate to the questions you need to edit, after completing your edits, `SUBMIT` the survey. 

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Edit%20a%20survey/resubmit.png)
