# API Compose Expressions

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

Uses javascript to allow you to send the survey, in it's entirety or in some modified form, to any publicly available API or to a non-public API which has been added to the SurveyStack Integrations.  In short - you can send your data where you want. For example, we use API compose expressions to push survey data into farmers FarmOS accounts in the Bionutrient Institute.
