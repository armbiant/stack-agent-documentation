# The Enrollment Process

This covers how project managers, assessors, data intermediaries, agronomists or other people supporting groups of producers can enroll farmers into their programs using **FarmOS instances** to store the farm data and the **Common Profile** to get basic data and connect to additional opportunities (models, loans, govt. programs) more easily in the future.

***NOTE: This process is currently in testing and will change. Expect ongoing improvements.

### Pre-work
**You should already have...**  
- Created a SurveyStack login  
- Read the [Best Practices guide](https://our-sci.gitlab.io/software/surveystack_tutorials/best_practices/)  
- Know how to [create a survey](https://our-sci.gitlab.io/software/surveystack_tutorials/create_new_survey/)  
- Understand what the [Question Set Library](https://our-sci.gitlab.io/software/surveystack_tutorials/QSL/) is and does  

### Video Walkthrough
<iframe width="560" height="315" src="https://www.youtube.com/embed/e9cndth2FtI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Step 1. Create a group  
Create a group for your organization.  You may want to have subgroups depending on your goals.  Learn more about creating groups and how you might structure your organization here.

1. When logged in, go to groups
2. Select create new group, name it, and click create
3. Group admins will be able to see private data and edit/resubmit surveys.
4. It is important to think about your group structure -> do you need subgroups?


### Step 2. Create your organizations enrollment form  
1. Create a new survey using the survey builder.  Ensure that you have assigned your group as the default by clicking on the 'edit' icon next to the group name and assigning your group.  This ensures that anyone who submits to this survey is submitting it to your group by default, which allows any group admins to view any private data, edit, resubmit, or reassign the survey if errors were made.  
2. Assign to your group.   
3. Add `common profile` from the Question Set Library. You can modify or hide some of the questions in this set if they are not relevant to your group, some things will be required. This data will be pushed to farmOS via API compose.  
4. Add `your custom` profile. Ask what questions you need for your organization that are missing from the common profile. If you want this data to be mapped and pushed to farmOS you can reach out to our-sci to get a walkthrough of API compose.   
> In order to map your custom profile to FarmOS, you'll need to write a little bit of code in the `advanced-->apiCompose` section of the survey.  Generally that's work that we help with.  It is not complicated, and once complete you can probably make small updates to the script without any help.  If you have someone with even junior level JS experience on your time we can also show you how this is done.
5. Save & publish your survey.  

### Step 3. Consider how you want to onboard people.   
Think about how you want to onboard people to your group. If you do this in person then you will submit on their behalf. If over email they will submit directly. The paths are slightly different, but both will work!  You can also mix and match, having more advanced producers do the work all over email and less tech-saavy producers give you the info over the phone or in person.

### Step 4. Invite Producer to SurveyStack and your group
1. Use the hamburger menu at the top left of surveystack to go to groups, select your group, and click `ADMIN`. If you are using subgroups you may want to invite users to subgroups vs. the top-level group.  
2. Scroll down to members, click invite, and add users via email.  
3. You can do this asynchronously, or you can do it with the farmer in real time (see step 3).  
4. They will get an email, they will click `JOIN`. 

### Step 5. Create FarmOS instance and assign to Producer
> For now, you will need `superadmin` access... that means we need to give you special rights in the platform.  Soon this will be available as a normal group admin, but for now please contact us to ensure you have `superadmin` status.  

1. Go to the hamburger menu and select `FarmOS`, under superadmin.
2. Go to create instance.  
3. Enter the required information: group, plan, farm name, address, timezone, and owner of the instance. You can add fields here if you want.  
   - Ensure you are adding the user to the correct group or subgroup.  If you are using subgroups and this producer is being onboarded into the subgroup, then select the subgroup.  Group permissions flow down - so top level group admins are automatically admins of lower groups.  Taht means data submitted to a subgroup is visible to the top level group admins, but NOT to sibling groups (which is probably what you want).
4. Agree to terms of service and register instance. Once completed you should see a message that says "Sucessfully Created Instance."  
5. Wait a few minutes and then go to `Users`, refresh your browser, and type in the user you just created an instance for. You should see a new line pop up with the name of the instance you just created.  
6. The user should get an email confirming the creation of their instance. They will not have to go to or use farmOS if they don't want to.

> BE CAREFUL!  You are a superadmin, so you can affect other people's groups and accounts.  Please only view or change your users.  If you have questions or make a mistake, contact us for help!

### Step 6. Fill out the enrollment survey  
- (if over email) Go to groups, admin, call for submissions. Select the enrollment survey and send it to users. This includes a magic link which means the user will not have to log into surveystack. 
- (if phone/in person) Go to farmOS -> users -> select yourself and map yourself to their farmOS instance (do NOT map yourself as the owner). Then fill out the enrollment survey on producer's behalf, making sure to select the correct farmOS account in the survey. 

### Step 7. Confirm data arrived
1. Go to farmOS instance, enter /profile/### at the end of the URL where ### is the ID of the profile (starts at 1 and goes up.)  In the initial submission it's always `/profile/1` for the common profile and `/profile/2` for your custom profile (if you have one).  
2. Go to Results page, confirm submission.  

### Fixing problems  
If you are an admin you can edit surveys by going to the `Results` page and selecting a submission.  
- To make changes to answers in a submission you can select `RESUBMIT`. This will take you back into the survey where you can make any corrections and then hit `SUBMIT` when you're done. 
- If the survey is assigned to the wrong group or user you can select `reassign`.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Enrollment/onboarding_editing_submissions.png)


