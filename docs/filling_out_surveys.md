# Complete a Survey

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

You can access surveys from your group's homescreen or from the `Browse` tab in the Side Menu.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Complete%20a%20survey/start_survey.png)

- Select `Start Survey` to begin the survey.
- Follow the prompts to navigate through the survey and answer questions.
- Some questions are **Private**, they will have a blue label (`This answer is private`). The answers to these questions are only visible to the person submitting the survey or the group administrator.
- Some questions are **Required**, they will have a red label (`Answer required`). You will not be able to select the `NEXT` button and advance through the survey until answer the question.
- Questions with checkboxes allow you to select multiple answers. 

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Complete%20a%20survey/multiple_choice_questions.png "examples of text, multiple choice, and checkbox questions with arrows pointing to private and required labels.")

- To answer dropdown questions, either select the down arrow in the answer box to open and scroll through the answer list *OR* start typing in the answer box and the app will auto-suggest responses.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Complete%20a%20survey/dropdown_questions.png)

- You can pick the location by selecting your location *OR* by moving the map so that the red pin in the center of the map is over the correct location.
     - If the red pin and your location do not match, you can select the `target` icon in the upper right corner of the map. 
- Once you have the red pin over the correct location, select the `PICK` button below the map.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Complete%20a%20survey/location_questions.png)

- To answer matrix questions
     - Select `Add row` and enter/select values for each column.
     - You can also delete and duplicate rows using the icons on the right side of the row.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Complete%20a%20survey/matrix_questions.png)

### Review a survey
- After you have completed the survey, the *survey outline* screen will give you the opportunity to review your answers before submitting the survey. 
     - green checkmarks mean you answered the question
     - an empty circle means that you did not answer the question
     - an orange circle with an exclamation point means that you did not answer a *required* question.
- You can also access the *survey outline* from any question screen by selecting the *outline* icon in the upper right corner of the question screen.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Complete%20a%20survey/survey_overview.png)

### Submit a survey
Once you are satisfied that the survey was completed correctly, you can select `SUBMIT` to submit the survey to the database. Please make sure you are logged into your account before submitting. This will make it easier to track down submissions in the future.  

- **Online Submission**: If you have internet connection, you should get a green message: `Successful Submission`

- **Offline Submission**: If you are **offline** you will get a red error message: `Error: Network Error`. Select `OK` and your submission will be **saved to your device as a draft**. To submit the survey:

    - When you have internet connection, go to `My Submissions` from the side menu.

    - If you see a `SUBMIT COMPLETED` button at the top of the page, there are completed surveys that are ready to submit (these drafts will have a `cloud` icon next to the submission).

    - Select the `SUBMIT COMPLETED`.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Complete%20a%20survey/submit_survey.png)

