# Welcome to Surveystack

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

SurveyStack is a flexible Progressive Web App that allows you to create surveys and scripts, manage communities, and collect and analyze data all in a single location.

SurveyStack is cross-platform, meaning that it works on computers and mobile devices, android and iPhone.

| Term | Definition |
|----|----------|
| Submission | Once you start filling out a survey, you have created a submission |
| Group | A group of users that are using a common set of surveys |
| Script | Javascript code that either runs an instrument <br> or calculates results from data in a survey or other data available on the web |
| Dashboard | Data visualization tool for survey results |

Click the help button at the top right of your surveystack screen at any point to return to this documentation.
