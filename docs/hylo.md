# Hylo Integration

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

## Description

[Hylo](https://www.hylo.com/) is a community platform for group sharing and communication. Like farmOS (and SurveyStack itself), Hylo has been developed in partnership with OpenTEAM with data privacy and farmer control in mind.

## Surveystack x Hylo
Hylo integration uses the same tools and structures as farmOS integration, [beginning with a Common Profile enrollment form](https://our-sci.gitlab.io/software/surveystack_tutorials/enrollment/). With the Common Profile, users can input farm profile details, such as location and crops produced, and send that information directly to farmOS and Hylo at the same time. 

[Common Enrollment Tutorial - Hylo](https://openteamag.gitlab.io/documentation/common-enrollment-guide/#1-integrating-hylo-and-surveystack)

The Hylo question set (also called the Hylo module) can be found [here](https://app.surveystack.io/surveys/61d3390f26a0dd00012a842b). Hylo uses a nested group structure that reflects the group structure used in SurveyStack, with members of child groups automatically added to parent groups upon creation. 

## Additional Resources

Hylo has extensive documentation on their site, and continually builds new resources based on user feedback. Get started in their help documents here: 
[Farm Profiles on Hylo](https://hylozoic.gitbook.io/hylo/guides/farm-profiles-on-hylo)

You can also join the [OpenTEAM Tech Feedback & Support group on Hylo](https://hylozoic.gitbook.io/hylo/guides/farm-profiles-on-hylo) and ask questions directly. 

OpenTEAM has created resources outlining the flow of information between SurveyStack, farmOS, Hylo, and other tools in the tech ecosystem here: [Access Tools and Support](https://openteam.community/access-tools-and-support/)

