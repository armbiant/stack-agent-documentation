### Create a Group

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

To create a group:  
1. Select `Groups` from the side menu  
2. Select `NEW...` from the upper right corner.  
3. Enter the `Group` name (the `Slug` will be created automatically from the group name).  
4. Click `CREATE`.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Group%20Features/create_group.png)

### Edit a Group
To edit a group, click on the `Admin` button in the upper right hand corner of the group page to open the **Group Editor**. 

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/Surveystack%20tutorials/Group%20Features/admin.png)